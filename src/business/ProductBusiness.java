package business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import bean.Movie;
import bean.Product;

@Stateless
@Local(ProductBusinessInterface.class)
@LocalBean
public class ProductBusiness implements ProductBusinessInterface {
	
	private List<Product> products;
	
    public ProductBusiness() {
    	products = new ArrayList<Product>();
    	products.add(new Product("25lb 2 ream", 20.00f, "Plain",  "Plain white paper"));
    	products.add(new Product("30lb 3 ream", 25.00f, "Cardstock", "Plain white cardstock"));
    	products.add(new Product("25lb 2 ream", 21.00f, "Plain", "Blue paper"));
    	products.add(new Product("15lb 2 ream", 15.00f, "Legal", "Plain white legal"));
    	products.add(new Product("20lb 3 ream", 18.00f, "Plain", "Plain white print"));
    }

    public int PurchaseProduct() {
			return 0;
    }
    
    public int AddProductToDatabase() {
			return 0;
    }
    
    public List<Product> getProductsByType() {
		// After Data Access is created, call to get a list of movies
		// based off of the genre.
		return products;
	}
    
}
