package business;

import bean.User;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

@Stateless
@Local(AccountBusinessInterface.class)
@LocalBean
@Alternative
public class AccountBusiness implements AccountBusinessInterface {
	
	public AccountBusiness() {
    }
	
    public int AuthenticateUser(User user) {
        if(user.getPassword().equals("abcde")) {
        	return 1;
        } else { 
        	return 0; }
    }
    
    public int RegisterUsers(User user) {
			return 0;
    }
    
    public String hashPassword(String pass) {
			return null;
    }

}
