package business;

import java.util.List;

import javax.ejb.Local;

import bean.Product;

@Local
public class ProductBusinessInterface {

	public int AddProductToDatabase();
	
	public int PurchaseProduct();
	
	public List<Product> getProductsByType();
	
}
