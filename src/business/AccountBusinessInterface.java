package business;

import javax.ejb.Local;
import bean.User;

@Local
public class AccountBusinessInterface {
	
	public int RegisterUsers(User user);
	
	public int AuthenticateUser(User user);
	
	String hashPassword(String pass);

}
