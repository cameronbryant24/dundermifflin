package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@ManagedBean
@ViewScoped
public class User {

	//Variables for the user
	@NotNull(message="You must put in your first name")
	@NotBlank(message="You must put in your first name")
	private String firstName;
	
	@NotNull(message="You must put in your last name")
	@NotBlank(message="You must put in your last name")
	private String lastName;
	
	@NotNull(message="You must put in a user name")
	@NotBlank(message="You must put in your username")
	@Size(min=3, max=20, message="Size must be between 3 and 20 characters long")
	private String userName;
	
	@NotNull(message="You must add an email")
	@NotBlank(message="You must add an email")
	@Size(max=100)
	@Email(message="This must be an email")
	private String email;
	
	@NotNull(message="You must put in a password")
	@NotBlank(message="You must put in a password")
	@Size(min=5, message="Size must be larger than 5")
	private String password;
	
	private int userId;
	
	private int CardID;
	
	public int MailingAdd;
	
	public int BillingAdd;
	
	/**
	 * Constructor for a managed bean.
	 * This constructor is meant to set the 
	 * attributes to a default value and not
	 * create objects throughout the code.
	 */
	public User() {
		super();
		firstName = "";
		lastName = "";
		userName = "";
		email = "";
		password = "";
		userId = 0;
		CardID = 0;
		MailingAdd = 1;
		BillingAdd = 1;
	}

	//Getters and setters
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Object getUserID() {
		return userId;
	}
	
	public void setUserId(int UserID) {
		this.userId = 0;
	}

	public Object getCardID() {
		return CardID;
	}
	
	public void setCardID(int CardID) {
		this.CardID = 0;
	}

	public Object getMailingAdd() {
		return MailingAdd;
	}
	
	public void setMailingAdd(int MailingAdd) {
		this.MailingAdd = 1;
	}

	public Object getBillingAdd() {
		return BillingAdd;
	}
	
	public void setBillingAdd(int BillingAdd) {
		this.BillingAdd = 1;
	}
}
