package bean;


public class Product {

	private String productName;
	private float buyPrice;
	private String type;
	private String description;
	
	public Product(String productName, float buyPrice, String type, String description) {
		super();
		this.productName = "";
		this.buyPrice = 0.f;
		this.type = "";
		this.description = "";
	}

	public Product(String productName, float buyPrice, String type, String description) {
		super();
		this.productName = productName;
		this.buyPrice = buyPrice;
		this.type = type;
		this.description = description;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public float getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(float buyPrice) {
		this.buyPrice = buyPrice;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
