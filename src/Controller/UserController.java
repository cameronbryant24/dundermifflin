package Controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import bean.User;
import business.AccountBusinessInterface;

@ManagedBean
@ViewScoped
public class UserController {

	@EJB
	AccountBusinessInterface ab;
	
	public String onLogin(User user) {
		if(ab.AuthenticateUser(user) == 1) {
			return "BrowseAll.xhtml";
		}
		else {
			return "Login.xhtml";
		}
	}
	
	public String onRegister(User user) {
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		return "Login.xhtml";
	}
}
