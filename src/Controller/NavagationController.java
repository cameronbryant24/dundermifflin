package Controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import business.ProductBusinessInterface;

@ManagedBean
@ViewScoped
public class NavagationController {

	@EJB
	ProductBusinessInterface productService;

	
	public ProductBusinessInterface getProductService() {
		return productService;
	}
}
