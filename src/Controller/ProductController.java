package Controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.Part;

import bean.Product;
import business.ProductBusiness;

@ManagedBean
@ViewScoped
public class ProductController {

	@Inject
	ProductBusiness productService;
	
	public String addProduct(Product p) {
		productService.AddProductToDatabase();
		return "BrowseAll.xhtml";
	}
	
	public ProductBusiness getProductService() {
		return productService;
	}
	
}
