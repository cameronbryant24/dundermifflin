package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;

import bean.User;
import exception.UserNotFoundException;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class UserDataAccess implements DataAccessInterface<User> {

	private Connection conn = null;
	private String url = "jdbc:mysql://localhost:3306/moviemadness";
	private String username = "Cameron";
	private String password = "123456";
	
	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findById(int id) throws UserNotFoundException {
		String sql = String.format("SELECT * FROM `user` WHERE `USER_ID`= %i",
				id);
		User dbUser = new User();
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			dbUser.setUserId(rs.getInt("ID"));
			dbUser.setFirstName(rs.getString("FIRSTNAME"));
			dbUser.setLastName(rs.getString("LASTNAME"));
			dbUser.setUserName(rs.getString("USERNAME"));
			dbUser.setEmail(rs.getString("EMAIL"));
			dbUser.setCardID(rs.getInt("CARD_ID"));
			dbUser.setMailingAdd(rs.getInt("MAILING_ADD"));
			dbUser.setBillingAdd(rs.getInt("BILLING_ADD"));
			dbUser.setPassword(rs.getString("PASSWORD"));
			return dbUser;
			
		} catch (SQLException e) {
			//e.printStackTrace();
			throw new UserNotFoundException();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean createT(User t) {
		try {
			String sql = String.format("INSERT INTO `user` "
					+ "(`FIRSTNAME`, `LASTNAME`, `USERNAME`, `EMAIL`, `PASSWORD`) "
					+ "VALUES ('%s', '%s', '%s', '%s', '%s');",
					t.getFirstName(),
					t.getLastName(),
					t.getUserName(),
					t.getEmail(),
					t.getPassword());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			String sql2 = "SELECT LAST_INSERT_ID() AS `LAST_ID` FROM `user`;";
			ResultSet rs = stmt.executeQuery(sql2);
			rs.next();
			int userID = rs.getInt("LAST_ID");
			stmt.close();
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("userID", userID);
			
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean updateT(User t) {
		try {
			String sql = String.format("UPDATE `user` SET `FIRSTNAME` = '%s', `LASTNAME` = '%s', `USERNAME` = '%s', `EMAIL` = '%s', `PASSWORD` = '%s' WHERE `USER_ID` = %d",
					t.getFirstName(),
					t.getLastName(),
					t.getUserName(),
					t.getEmail(),
					t.getPassword(),
					t.getUserID());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean deleteT(User t) {
		String sql = String.format("DELETE FROM `user` WHERE `USER_ID` = %d",
				t.getUserID());
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Find the user profile by username.Could be used for logins.
	 * @param User
	 * @return User
	 */
	public User findByUserName(User t) {
		try {
			String sql = String.format("SELECT * FROM `user` WHERE `USERNAME`= '%s';",
					t.getUserName());
			User dbUser = new User();
			
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			dbUser.setUserId(rs.getInt("USER_ID"));
			dbUser.setFirstName(rs.getString("FIRSTNAME"));
			dbUser.setLastName(rs.getString("LASTNAME"));
			dbUser.setUserName(rs.getString("USERNAME"));
			dbUser.setEmail(rs.getString("EMAIL"));
			dbUser.setCardID(rs.getInt("CARD_ID"));
			dbUser.setMailingAdd(rs.getInt("MAILING_ADD"));
			dbUser.setPassword(rs.getString("PASSWORD"));
			return dbUser;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return new User();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
