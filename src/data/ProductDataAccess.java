package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import bean.Product;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class ProductDataAccess implements DataAccessInterface<Product> {

	private Connection conn = null;
	private String url="jdbc:mysql://localhost:3306/moviemadness";
	private String username = "Cameron";
	private String password = "123456";
	
	@Override
	public List<Product> findAll() {
		String sql = "SELECT * FROM `product` ORDER BY `TYPE`;";
		List<Product> products = new ArrayList<Product>();
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				products.add(new Product(
						rs.getString("PRODUCT_NAME"),
						rs.getFloat("PURCHASE_PRICE"),
						rs.getString("TYPE"),
						rs.getString("DESCRIPTION")
						));
			}
			return products;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public Product findById(int id) {
		String sql = String.format("SELECT * FROM `product` WHERE `PRODUCT_ID`= %i", id);
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			return null;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean createT(Product t) {
		try {
			String sql = String.format("INSERT INTO `movie` (`PRODUCT_NAME`, `PURCHASE_PRICE`, `TYPE`, `DESCRIPTION`)"
					+ "VALUES ('%s', %f, '%s', '%s');",
					t.getProductName(),
					t.getBuyPrice(),
					t.getType(),
					t.getDescription());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean updateT(Product t) {
		String sql = String.format("");
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean deleteT(Product t) {
		String sql = String.format("");
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<Product> getByType(String type){
		String sql = String.format("SELECT * FROM `product` WHERE `TYPE` = '%s';", type);
		List<Product> products = new ArrayList<Product>();
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				products.add(new Product(
						rs.getString("PRODUCT_NAME"),
						rs.getFloat("PURCHASE_PRICE"),
						rs.getString("TYPE"),
						rs.getString("DESCRIPTION")));
			}
			return products;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<String> getTypes(){
		String sql = "SELECT DISTINCT `GENRE` FROM `product`;";
		List<String> types = new ArrayList<String>();
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				types.add(rs.getString("TYPE"));
			}
			return types;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
