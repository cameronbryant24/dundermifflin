package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import bean.MailAddress;
import bean.User;
import exception.MailAddressNotFoundException;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class MailAddressDataAccess implements DataAccessInterface<MailAddress> {

	private Connection conn = null;
	private String url = "jdbc:mysql://localhost:3306/moviemadness";
	private String username = "Cameron";
	private String password = "123456";
	
	@Override
	public List<MailAddress> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Pass in a neutral variable, this method will get the sessions user Id and return
	 * The Mailing address of this user.
	 * @param id
	 * @return MailAddress
	 * @throws MailAddressNotFoundException 
	 */
	@Override
	public MailAddress findById(int id) throws MailAddressNotFoundException {
		try {
			User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
			String sql = String.format("SELECT `MAILING_ADD` FROM `user` WHERE `USER_ID` = %d;", user.getUserID());
			
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			int mailAdd = rs.getInt("MAILING_ADD");
			
			
			String sql2 = String.format("SELECT * FROM `mailingaddress` WHERE `MAILING_ID`= %d;", mailAdd);
			Statement stmt2 = conn.createStatement();
			ResultSet rs2 = stmt2.executeQuery(sql2);
			rs2.next();
			MailAddress ma = new MailAddress();
			ma.setMailId(mailAdd);
			ma.setAddress1(rs2.getString("ADDRESS_1"));
			ma.setAddress2(rs2.getString("ADDRESS_2"));
			ma.setAddress3(rs2.getString("ADDRESS_3"));
			ma.setCountry(rs2.getString("COUNTRY"));
			ma.setCity(rs2.getString("CITY"));
			ma.setState(rs2.getString("STATE"));
			ma.setZip(rs2.getString("ZIP"));
			
			stmt.close();
			stmt2.close();
			return ma;
			
		} catch (SQLException e) {
			//e.printStackTrace();
			throw new MailAddressNotFoundException();
		}finally {
		
			
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean createT(MailAddress t) {
		try {
			String sql = String.format("INSERT INTO `mailingaddress` (`ADDRESS_1`, `ADDRESS_2`, `ADDRESS_3`, `COUNTRY`, `CITY`, `STATE`, `ZIP`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s');",
					t.getAddress1(),
					t.getAddress2(),
					t.getAddress3(),
					t.getCountry(),
					t.getCity(),
					t.getState(),
					t.getZip());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			
			String sql2 = "SELECT LAST_INSERT_ID() AS `LAST_ID` FROM `mailingaddress`;";
			ResultSet rs = stmt.executeQuery(sql2);
			rs.next();
			int mailID = rs.getInt("LAST_ID");
			stmt.close();
			User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("mailId", mailID);
			Statement stmt2 = conn.createStatement();
			String sql3 = String.format("UPDATE `user` SET `MAILING_ADD` = %d WHERE `USER_ID` = %d;", mailID, user.getUserID());
			stmt2.executeUpdate(sql3);
			stmt2.close();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean updateT(MailAddress t) {
		try {
			String sql = String.format("UPDATE `mailingaddress` SET `ADDRESS_1` = '%s', `ADDRESS_2` = '%s', `ADDRESS_3` = '%s', `COUNTRY` = '%s', `CITY` = '%s', `STATE` = '%s', `ZIP` = '%s' WHERE `MAILING_ID` = %d",
					t.getAddress1(),
					t.getAddress2(),
					t.getAddress3(),
					t.getCountry(),
					t.getCity(),
					t.getState(),
					t.getZip(),
					t.getMailId());
			System.out.println(sql);
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean deleteT(MailAddress t) {
		// TODO Auto-generated method stub
		return false;
	}

}
