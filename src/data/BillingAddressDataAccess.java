package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;

import bean.BillingAddress;
import bean.CreditCard;
import bean.User;
import exception.BillingAddressNotFoundException;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class BillingAddressDataAccess implements DataAccessInterface<BillingAddress> {

	private Connection conn = null;
	private String url="jdbc:mysql://localhost:3306/moviemadness";
	private String username = "Cameron";
	private String password = "123456";
	
	@Override
	public List<BillingAddress> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BillingAddress findById(int id) throws BillingAddressNotFoundException {
		try {
			CreditCard card = (CreditCard) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("card");
			String sql = String.format("SELECT `BILLING_ADDRESS` FROM `payment` WHERE `CARD_ID` = %d;", card.getCardId());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			int billId = rs.getInt("BILLING_ADDRESS");
			String sql2 = String.format("SELECT * FROM `billingaddress` WHERE `BILLING_ID`= %d;", billId);
			System.out.println(sql2);
			Statement stmt2 = conn.createStatement();
			ResultSet rs2 = stmt2.executeQuery(sql2);
			rs2.next();
			BillingAddress b = new BillingAddress();
			b.setBillingId(rs2.getInt("BILLING_ID"));
			b.setAddress1(rs2.getString("ADDRESS_1"));
			b.setAddress2(rs2.getString("ADDRESS_2"));
			b.setAddress3(rs2.getString("ADDRESS_3"));
			b.setCountry(rs2.getString("COUNTRY"));
			b.setCity(rs2.getString("CITY"));
			b.setState(rs2.getString("STATE"));
			b.setZip(rs2.getString("ZIP"));
			
			stmt.close();
			stmt2.close();
			return b;
			
		} catch (SQLException | NullPointerException e) {
			//e.printStackTrace();
			throw new BillingAddressNotFoundException();
		}finally {
		
			
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean createT(BillingAddress t) {
		try {
			String sql = String.format("INSERT INTO `billingaddress` (`ADDRESS_1`, `ADDRESS_2`, `ADDRESS_3`, `COUNTRY`, `CITY`, `STATE`, `ZIP`)"
					+ "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s');",
					t.getAddress1(),
					t.getAddress2(),
					t.getAddress3(),
					t.getCountry(),
					t.getCity(),
					t.getState(),
					t.getZip());
			
			
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			
			String sql2 = "SELECT LAST_INSERT_ID() AS `LAST_ID` FROM `billingaddress`;";
			ResultSet rs = stmt.executeQuery(sql2);
			rs.next();
			int billID = rs.getInt("LAST_ID");
			stmt.close();
			CreditCard card = (CreditCard) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("card");
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("billId", billID);
			Statement stmt2 = conn.createStatement();
			String sql3 = String.format("UPDATE `payment` SET `BILLING_ADDRESS` = %d WHERE `CARD_ID` = %d;", billID, card.getCardId());
			stmt2.executeUpdate(sql3);
			stmt2.close();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean updateT(BillingAddress t) {
		try {
			String sql = String.format("UPDATE `billingaddress` SET `ADDRESS_1` = '%s', `ADDRESS_2` = '%s', `ADDRESS_3` = '%s', `COUNTRY` = '%s', `CITY` = '%s', `STATE` = '%s', `ZIP` = '%s' WHERE `BILLING_ID` = %d",
					t.getAddress1(),
					t.getAddress2(),
					t.getAddress3(),
					t.getCountry(),
					t.getCity(),
					t.getState(),
					t.getZip(),
					t.getBillingId());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean deleteT(BillingAddress t) {
		// TODO Auto-generated method stub
		return false;
	}

}
